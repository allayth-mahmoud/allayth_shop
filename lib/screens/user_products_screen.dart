import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';
import '../widgets/user_product_item.dart';
import '../widgets/app_drawer.dart';
import './edit_product_screen.dart';

class UserProductsScreen extends StatelessWidget {
  static const routeName = '/user-products';

  Future<void> _refreshProducts(BuildContext context) async {
    await Provider.of<Products>(context, listen: false)
        .fetchAndSetProducts(true);
  }

  @override
  Widget build(BuildContext context) {
    var media = MediaQuery.of(context);
    // final productsData = Provider.of<Products>(context);
    print('rebuilding...');
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Center(
            child: const Text(
          'Your Products',
          style: TextStyle(fontWeight: FontWeight.bold),
        )),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(EditProductScreen.routeName);
            },
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: Column(
        children: [
          SizedBox(
            height: media.size.height * 0.27,
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(22)),
                child: Image.asset(
                  'assets/images/image3.jpg',
                  fit: BoxFit.cover,
                )),
          ),
          SizedBox(
            height: media.size.height * 0.47,

            child: FutureBuilder(
              future: _refreshProducts(context),
              builder: (ctx, snapshot) =>
                  snapshot.connectionState == ConnectionState.waiting
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      :
                  RefreshIndicator(
                          onRefresh: () => _refreshProducts(context),
                          child: Consumer<Products>(
                            builder: (ctx, productsData, _) => Padding(
                              padding: EdgeInsets.all(8),
                              child: productsData.items.length != 00?
                              ListView.builder(
                                itemCount: productsData.items.length,
                                itemBuilder: (_, i) => Column(
                                  children: [
                                    UserProductItem(
                                      productsData.items[i].id,
                                      productsData.items[i].title,
                                      productsData.items[i].imageUrl,
                                    ),
                                    Divider(),
                                  ],
                                ),
                              ): Center(child: Text('You have no orders yet!'),),
                            ),
                          ),
                        )
            ),
          ),
        ],
      ),
    );
  }
}
