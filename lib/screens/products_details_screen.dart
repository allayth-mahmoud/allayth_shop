import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';

class ProductsDetailsScreen extends StatelessWidget {
  static const routeName = '/products_details_screen';

  @override
  Widget build(BuildContext context) {
    final productId =  ModalRoute.of(context)?.settings.arguments as String; // this is id
    final loadedProduct = Provider.of<Products>(context, listen: false)
        .findById(productId);
    return Scaffold(
      appBar: AppBar(
        actions: [IconButton(
          icon: Icon(Icons.shopping_cart, color: Colors.transparent,),
          onPressed: () {
          },
        ),],
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Center(child: Text(loadedProduct.title, style: TextStyle(fontWeight: FontWeight.bold),)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                loadedProduct.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(height: 10),
            Text(
              '\$${loadedProduct.price}',
              style: TextStyle(
                color: Colors.grey,
                fontSize: 20,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              child: Text(
                loadedProduct.description,
                textAlign: TextAlign.center,
                softWrap: true,
              ),
            )
          ],
        ),
      ),
    );
  }
}
