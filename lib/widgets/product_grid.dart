import '../providers/products.dart';
import '../widgets/product_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class ProductGrid extends StatelessWidget {
bool  showOnlyFavorites;
ProductGrid(this.showOnlyFavorites);
  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final products = showOnlyFavorites? productsData.favoriteItems: productsData.items;
    print(products.length);
    return   GridView.builder(
        padding: const EdgeInsets.all(10.0),
        itemCount: products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
        itemBuilder: (ctx, i) => ChangeNotifierProvider.value(
          //create: (c) => products[i],
          value: products[i],
          child: ProductItem(
            // products[i].id,
            // products[i].title,
            // products[i].imageUrl,
          ),
        ),

    );
  }
}
